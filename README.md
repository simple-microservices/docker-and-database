# docker-and-database

Pertama disini saya menggunakan JDK : 17 , MYSQL 8 dengan port 3310 

Langkah pertama :
- Create database: db_product_services
- Create database: db_payment_service
- Create database: db_order_service

------

SERVICE-REGISTRY: Service registry adalah sebuah komponen yang digunakan dalam arsitektur mikroservis untuk memantau dan menemukan layanan yang tersedia. Dalam konteks Spring Boot, Service Registry dapat berperan sebagai registry dimana mikroservis dalam suatu aplikasi dapat mendaftarkan dirinya sendiri, serta mencari dan berkomunikasi dengan mikroservis lainnya.


-----

CONFIG-SERVER : Khusus config server, ini perlu di daftarkan manual mas ke sevice-registry, karena dia baru ngeclone config dari git setelah aplikasi config-server ini dijalankan.
Jadi kalau semisal kita tidak kasi env manual untuk di daftarin ke service registry maka dia tidak akan terdaftar ke dalam sana.

----

CONFIG-GIT : Berfungsi untuk mengambil settingan config application.yaml yang berada di dalam GIT


-----

CLOUD-GATEWAY:  Penghubung Antara Service Service Lain Nya. Jadi Dari Front End Hanya Menghit Ke Api-Gateway Jadi Sentral Ke Api Gateway

-----

ORDER-SERVICE : Service untuk orders

-----

PAYMENT-SERVICE : Service untuk payments 


-----


PRODUCT-SERVICE : Service untuk products 


------

LANGKAH - LANGKAH RUNNING VIA DOCKER :
•	Dijadikan JAR Semua Project Java Nya.
•	Lalu Dibuatkan File Masing Masing Docker File Nya.
•	Lalu Di Build Menjadi Docker Image Masing Masing Project Nya.
•	Lalu Buat Docker Compose Untuk Running Docker Image Nya.
Syntaks untuk menjadi  docker image :
-	Arahkan ke folder project masing masing melalui command line
-	Lalu ketik syntax example : docker build -t syafri/cloudgateway:0.0.1 .
-	docker build -t [namaId DockerHub]/[nama aplikasi]:[tag image (versi aplikasi)] .

-------

Urutan running service nya dari yang pertama sampai yang terakhir:
-	SERVICE-REGISTRY
-	CONFIG-SERVICE
-	CLOUD-GATEWAY
-	PRODUCT-SERVICE
-	ORDER-SERVICE
-	PAYMENT-SERVICE

Actuator : ngecek keadaan aplikasi saat ini, apakah aplikasi ini sudah running dengan semesti nya.

-----


Healthcheck : apakah dia jalan sampai tahap akhir

-------

Syntaks docker:
-	Docker images
-	Untuk up services : docker-compose -f docker-compose.yml up -d
-	Untuk down services : docker-compose -f docker-compose.yml down
-	Untuk delete docker images : docker image rm c33840e63306
-	Untuk build docker image: docker build -t syafri/serviceregistry:0.0.1 .




